import ddf.minim.*;

Minim minim;
AudioPlayer player;
AudioPlayer player2;

float a=PI;
Float b=PI;
PShape circle;
PShape circle2;
PShape arco;
PShape arco2;

int xboton1 = 200;
int yboton1 = 200;
int xboton2 = 200;
int yboton2 = 500;

int xarco = 750;
int yarco = 300;
int xarco2 = 750;
int yarco2 = 600;


int radioboton = 150;

void setup() {  
  size(1000, 700, P2D);
  
  minim = new Minim(this);
  player = minim.loadFile("alarm.mp3");
  player2 = minim.loadFile("alarm.mp3");
  
  circle = createShape(ELLIPSE, xboton1, yboton1, radioboton, radioboton);
  circle.setStroke(color(192,192,192));  
  
  circle2 = createShape(ELLIPSE, xboton2, yboton2, radioboton, radioboton);
  circle2.setStroke(color(192,192,192));
  
  arco = createShape(ARC, xarco, yarco, 400, 400,PI, TWO_PI,CHORD);
  arco.setStroke(color(0));

  arco2 = createShape(ARC, xarco2, yarco2, 400, 400,PI, TWO_PI,CHORD);
  arco2.setStroke(color(0));

  smooth();
}

void draw() {
  background(51,255,51);
 
  float d1 = dist(mouseX, mouseY, xboton1, yboton1);
  float d2 = dist(mouseX, mouseY, xboton2, yboton2);
  
  
  //Boton 1 temperatura
  if (d1+75 <= radioboton){
    circle.setFill(color(255,0,0));
    shape(circle);
    a=a+exp(a)/10000;
      if (a>=TWO_PI){
      a=TWO_PI;
    }
  }
  else if((radioboton<d1+75) && (a>PI)){
      a=a-exp(a)/10000;
      circle.setFill(color(204,229,255));
      shape(circle);
  }
  else{
     circle.setFill(color(255));
     shape(circle);
  }
  
  //Boton 2 Presion
  if (d2+75 <= radioboton){
    circle2.setFill(color(255,255,0));
    shape(circle2);
    b=b+log(b)/500;
      if (b>=TWO_PI){
      b=TWO_PI;
    }
  }
  else if((radioboton<d2+75) && (b>PI)){
      b=b-log(b)/500;
      circle2.setFill(color(204,229,255));
      shape(circle2);
  }
  else{
     circle2.setFill(color(255));
     shape(circle2);
  }
  

//Arcos para medidores
  arco.setFill(color(255));
  shape(arco);
  
  arco2.setFill(color(255));
  shape(arco2);

//yyy

//Textos y valores
float val = (a*100/PI)-100;
String valor_numerico = nfc(val,1)+" ºC";
if (val<75){
   if ( player.isPlaying()) 
   {player.pause();
 player.rewind();}
 }
if (val>=75){
    arco.setFill(color(255,0,0));
  shape(arco);
   player.play();}
   

float val2 = (b*1000/PI-1000);
String valor_numerico2 = nfc(val2,1)+ " Pa";
if (val2<750){
  if ( player2.isPlaying()) 
   {player2.pause();
 player2.rewind();}
 }
if (val2>=750){
  arco2.setFill(color(255,0,0));
  shape(arco2);
  player2.play();
}
strokeWeight(5);
line(xarco,yarco,(xarco+cos(a)*200),(yarco+sin(a)*200));

strokeWeight(5);
line(xarco2,yarco2,xarco2+cos(b)*200,yarco2+sin(b)*200);

 textSize(34);
 fill(0);
 text(valor_numerico,700,365);

 textSize(34);
 fill(0);
 text(valor_numerico2,700,665);
 
 textSize(34);
 fill(0);
 text("Temperatura",105,320);
 
 textSize(34);
 fill(0);
 text("Presión",145,610);
 
 textSize(34);
 fill(0);
 text("Medida Temperatura",602,330);
 
 textSize(34);
 fill(0);
 text("Medida Presión",630,630);
}
